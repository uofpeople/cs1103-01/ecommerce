package com.ecommerce.orders;

import java.util.List;

import com.ecommerce.Customer;
import com.ecommerce.Product;

public class Order {
    private String orderID;
    private Customer customer;
    private List<Product> products;
    private double orderTotal;

    //Constructor
    public Order(String orderID, Customer customer, List<Product> products) {
        this.orderID = orderID;
        this.customer = customer;
        this.products = products;
        this.orderTotal = calculateOrderTotal();
    }
    
    //Method to calculate the order total
    private double calculateOrderTotal() {
        double total = 0;
        for (Product product : products){
            total += product.getPrice();
        }
        return total;
    }

    //Method to generate order summary
    public String generateOrderSummary() {
        StringBuilder summary = new StringBuilder("Order ID: " + orderID + "\nCustomer: " + customer.getName() + "\nProducts:\n");
        for (Product product : products){
            summary.append(product.getName()).append(" - $").append(product.getPrice()).append("\n"); 
        }
        summary.append("Total: $").append(orderTotal);
        return summary.toString();
    }

    // Getters
    public String getOrderID() { return orderID; }
    public Customer getCustomer() { return customer; }
    public List<Product> getProducts() { return products; }
    public double getOrderTotal() { return orderTotal; }
}
