package com.ecommerce;

import java.util.ArrayList;
import java.util.List;

import com.ecommerce.orders.Order;


public class Customer {
    private String customerID;    
    private String name;
    private List<Product> shoppingCart;

    //Constructor
    public Customer(String customerID, String name) {
        this.customerID = customerID;
        this.name = name;
        this.shoppingCart = new ArrayList<>();
    }

    //Methods to add and remove products from the shopping cart
    public void addProductToCart(Product product) {
        shoppingCart.add(product);
    }

    public void removeProductFromCart(Product product) {
        shoppingCart.remove(product);
    }

    //Calculate the total cost
    public double CalculateTotalCost() {
        double total = 0;
        for (Product product : shoppingCart) {
            total += product.getPrice();
        }
        return total;
    }

    //Place an order
    public Order placeOrder() {
        return new Order(generateOrderID(), this, new ArrayList<>(shoppingCart));
    }

    //Helper method to generate an order ID
    private String generateOrderID() {
        return "ORDER " + System.currentTimeMillis(); 
    }

    // Getters and Setters
    public String getCustomerID() { return customerID; }
    public void setCustomerID(String customerID) { this.customerID = customerID; }
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
}
