
import com.ecommerce.*;
import com.ecommerce.orders.*;

public class Ecommerce {
    public static void main(String[] args){
        //Create instances of products
        Product product01 = new Product("P001", "Iphone", 799.99);
        Product product02 = new Product("P002", "MacBookPro", 1999.99);

        // Create an instance of a custumer
        Customer customer = new Customer("C001", "John");

        //Customer browses products and adds them to the shopping cart
        customer.addProductToCart(product01);
        customer.addProductToCart(product02);

        //Customer places an order
        Order order = customer.placeOrder();

        //Display information about products, customer, and order
        System.out.println("Products: ");
        System.out.println(product01.getName() + " - $" + product01.getPrice());
        System.out.println(product02.getName() + " - $" + product02.getPrice());

        System.out.println("\nCustomer:");
        System.out.println("Customer ID: " + customer.getCustomerID());
        System.out.println("Name: " + customer.getName());

        System.out.println("\nOrder Summary:");
        System.out.println(order.generateOrderSummary());
    }
}
